<?php
/**
 * Plugin Name: MTG Decklist
 * Author: David Buchmann
 * Version: 1.0.0
 */
  
function loadMtGBlock() {
  wp_enqueue_script(
    'mtg-decklist-block',
    plugin_dir_url(__FILE__) . 'mtg-decklist-block.js',
    array('wp-blocks','wp-editor'),
    true
  );
}
   
add_action('enqueue_block_editor_assets', 'loadMtGBlock');
