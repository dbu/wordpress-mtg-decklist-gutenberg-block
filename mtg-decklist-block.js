wp.blocks.registerBlockType('mtg/decklist', {
  title: 'MtG Decklist',
  icon: 'menu',
  category: 'common',
  attributes: {
    decklist: {type: 'string'}
  },

/* This configures how the decklist field will work, and sets up the necessary elements */

  edit: function(props) {
    function updateContent(event) {
      props.setAttributes({decklist: event.target.value})
    }
    return React.createElement(
      "div",
      null,
      React.createElement(
        "h3",
        null,
        "MtG Decklist"
      ),
      React.createElement("textarea", { rows: 20, columns: 120, style: {width: "100%"}, value: props.attributes.decklist, onChange: updateContent }),
    );
  },
  save: function(props) {
    return wp.element.createElement(
      "div",
      null,
      "[decklist]\n"+props.attributes.decklist+"\n[/decklist]"
    );
  }
})
